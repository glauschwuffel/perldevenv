requires 'perl' => 5.010001;
requires 'Carp' => 0;
requires 'Class::Method::Modifiers' => 0;
requires 'Crypt::Eksblowfish::Bcrypt' => 0;
requires 'Crypt::SaltedHash' => 0;
requires 'Data::UUID' => 0;
requires 'Data::Validate::Email' => 0;
requires 'Data::XLSX::Parser' => 0;
requires 'Dist::Zilla::App::Command::cover' => 0;
requires 'DBD::Pg' => 0;
requires 'DBD::SQLite' => 0;
requires 'DBIx::Class' => 0;
requires 'DBIx::Class::DeploymentHandler' => 0;
requires 'Digest' => 0;
requires 'Digest::SHA1' => 0;
requires 'Exporter::Auto' => 0;
requires 'Fcntl' => 0;
requires 'File::Basename' => 0;
requires 'File::Find::Rule' => 0;
requires 'File::Spec' => 0;
requires 'Getopt::Long' => 0;
requires 'Hash::Merge' => 0;
requires 'JSON::XS' => 0;
requires 'LaTeX::Driver' => 0;
requires 'Lingua::DE::ASCII' => 0;
requires 'List::Util' => 0;
requires 'Minion' => 0;
requires 'Minion::Notifier' => 0;
requires 'Module::Path' => 0;
requires 'Module::Load' => 0;
requires 'Mojo::Pg' => 0;
requires 'Mojo::Role' => 0;
requires 'Mojolicious' => 5.67;
requires 'Mojolicious::Plugin::AdditionalValidationChecks' => 0;
requires 'Mojolicious::Plugin::BasicAuth' => 0;
requires 'Mojolicious::Plugin::CountryDropDown' => 0;
requires 'Mojolicious::Plugin::CSSLoader' => 0;
requires 'Mojolicious::Plugin::FormFieldsFromJSON' => 0.30;
requires 'Mojolicious::Plugin::I18N' => 0;
requires 'Mojolicious::Plugin::I18NUtils' => ;
requires 'Mojolicious::Plugin::JSLoader' => 0;
requires 'Mojolicious::Plugin::MoreHTMLHelpers' => 0;
requires 'Mojolicious::Plugin::TagHelpersI18N' => 0;
requires 'Mojolicious::Plugin::WebAPI' => 0;
requires 'Moo' => 0;
requires 'MySQL::Workbench::DBIC' => 0;
requires 'MySQL::Workbench::SQLiteSimple' => 0;
requires 'OpenAPI::Client' => 0;
requires 'Pandoc' => 0;
requires 'Role::Tiny' => 0;
requires 'String::Random' => 0;
requires 'System::Command' => 0;
requires 'Time::Piece' => 0;
requires 'Test::Mojo::WithRoles' => 0;
requires 'Test::Mojo::Role::TestDeep' => 0;
requires 'TeX::Encode' => 0;
requires 'UUID::Tiny' => 0;
requires 'WebService::MIAB' => 0.03;
