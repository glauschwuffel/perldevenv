(work in progress)

Ein auf Ubuntu basierendes Image Entwicklung von Modernem Perl.

Das Image enthält Perl und eine Vielzahl von Modulen, unter anderem:

- [Dist::Zilla](http://dzil.org/ "Webseite von Dist::Zilla")a für die einfache Entwicklung von Perl-Software, die mit dem
  Perl-Werkzeugkasten verarbeitet werden kann.
- cpanminus und perlcritic als Bestandteile des Werkzeugkastens
- Die Objektsysteme Moose und Moo
- Mojolicious, Minion und ihre Anbindung an Postgresql
- Postgres selbst und DBIx::Class
- [Test::Spec](https://metacpan.org/pod/Test::Spec "Module documentation for Test::Spec") für Tests, die an[ RSpec](http://rspec.info/ "Webseite von RSpec") angelehnt sind
- Test::BDD::Cucumber zum Schreiben von an Cucumber angelehnten Akzeptanztests (https://metacpan.org/pod/Test::BDD::Cucumber)
- PPI zum Parsen von Perl-Quellen (https://metacpan.org/pod/PPI)
- XeTeX zum Erstellen ansehnlicher Dokumente
