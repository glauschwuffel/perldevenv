FROM ubuntu:rolling

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get clean
RUN apt-get update && apt-get install -y \
  build-essential \
  cpanminus \
  curl \
  libcrypt-eksblowfish-perl \
  libdata-uuid-perl \
  libdatetime-format-strptime-perl \
  libdatetime-timezone-perl \
  libdbd-pg-perl \
  libdbd-sqlite3-perl \
  libdbix-class-deploymenthandler-perl \
  libdbix-class-perl \
  libdigest-sha-perl \
  libdist-zilla-app-command-cover-perl \
  libdist-zilla-perl \
  libdist-zilla-plugin-git-perl \
  libdist-zilla-plugin-podweaver-perl \
  libdist-zilla-plugin-test-notabs-perl \
  libdist-zilla-plugin-test-perl-critic-perl \
  libfile-find-rule-perl \
  libjson-xs-perl \
  libminion-perl \
  libmodule-path-perl \
  libmojo-pg-perl \
  libmojolicious-perl \
  libmoo-perl \
  libmoosex-types-path-tiny-perl \
  libppi-perl \
  libppix-regexp-perl \
  libpq-dev \
  libtest-bdd-cucumber-perl \
  libtest-exception-perl \
  libtest-longstring-perl \
  libtest-net-ldap-perl \
  libtest-notabs-perl \
  libtest-pod-perl \
  libtest-spec-perl \
  libtest-warn-perl \
  libtest-warnings-perl \
  libtest2-suite-perl \
  libxml-libxml-perl \
  net-tools \
  procps \
  tzdata \
  unzip

RUN cpanm Test::Class::Moose

# Set entrypoint so Gitlab is happy.
# See https://gitlab.com/gitlab-org/gitlab-runner/issues/1170 why this is needed
ENTRYPOINT [""]
